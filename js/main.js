let req = new XMLHttpRequest();

let root = document.getElementById('root');
let logo = document.createElement('img');
logo.src = './img/logo.png';
logo.setAttribute('id', 'logo');

root.appendChild(logo);

let container = document.createElement('div');
container.setAttribute('class', 'conteneur');
root.appendChild(container);

req.open('GET', 'http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline', true);

req.onload = function () {
    let data = JSON.parse( this.response );

    if (req.status >= 200 && req.status < 400) {

        data.forEach( (makeup) => {
            let produit = document.createElement('div');
            produit.setAttribute('id', 'produit');

            let image = document.createElement('img');
            image.src = makeup.image_link;
            image.setAttribute('id', 'image');

            let h1 = document.createElement('h2');
            h1.textContent = makeup.name;
            h1.setAttribute('id', 'name');

            let p = document.createElement('p');
            makeup.description = makeup.description.substring(0, 100);
            p.setAttribute('id', 'description');

            p.textContent = `${makeup.description}...`;

            let price = document.createElement('p');
            price.textContent = `${makeup.price}€`;
            price.setAttribute('id', 'prix')


            produit.appendChild(image);
            produit.appendChild(h1);
            produit.appendChild(p);
            produit.appendChild(price);
            container.appendChild(produit);
        });
    }else {
        console.log('erreur');
    }
}

req.send();